import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';

// Views
import HomeView from './views/Home.vue';
import AboutView from './views/About.vue';

Vue.use(VueRouter)

const routes = [
    {
        path: '/about',
        component: AboutView
    },
    {
        path: '/',
        component: HomeView
    }
]

const router = new VueRouter({
    routes: routes,
    mode: 'history'
})

new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
